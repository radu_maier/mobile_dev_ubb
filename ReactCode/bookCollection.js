import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert,
  TextInput,
  ListView,
  TouchableOpacity,
  TouchableHighlight,
  AsyncStorage
} from 'react-native';

'use strict';
const ReactNative = require('react-native');
import Button from 'react-native-button';
import Book from './Book.js';

export default class BookCollection extends Component {
  // Initialize the hardcoded data
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    var book = new Book(1, 'C++ for Dummies', 'Mike Stan');
    var books = [book];

    this.state = {
      dataSource: ds.cloneWithRows(books),
      newTitle: 'new title...',
      newAuthor: 'new author...'
    };

    AsyncStorage.getItem('@AwesomeProject:allBooks').then((value) => {
      if (value != null) {
        this.setState({ dataSource: ds.cloneWithRows(JSON.parse(value)) });

      } else {
        this.setState({ dataSource: ds.cloneWithRows([new Book(1, 'Ruby for Dummies', 'Mike Stan')]) });
      }
      //alert('AsyncStorage');
    }).done();


  }

  getNextId() {
    let maxId = 0;
    for (var i = 0; i < this.state.dataSource.getRowCount(); ++i) {
     let crtBook = this.state.dataSource.getRowData(0, i);
     if(crtBook.id > maxId){
       maxId = crtBook.id;
     }
    }
    return maxId + 1;
  }

  saveBook() {
    //alert('pushed!');
    var title = this.state.newTitle;
    var author = this.state.author;
    var book = new Book(this.getNextId(), title, author);
    var tempBooks = [];
    for (var i = 0; i < this.state.dataSource.getRowCount(); ++i) {
      tempBooks.push(this.state.dataSource.getRowData(0, i));
    }
    tempBooks.push(book);
    this.setState({ dataSource: this.state.dataSource.cloneWithRows(tempBooks) });
    AsyncStorage.setItem('@AwesomeProject:allBooks', JSON.stringify(tempBooks));
  }

  deleteBook(book) {
    var tempBooks = [];
    for (var i = 0; i < this.state.dataSource.getRowCount(); ++i) {
      let crtBook = this.state.dataSource.getRowData(0, i);
      if (crtBook.id !== book.id) {
        tempBooks.push(crtBook);
      }
    }
    this.setState({ dataSource: this.state.dataSource.cloneWithRows(tempBooks) });
    AsyncStorage.setItem('@AwesomeProject:allBooks', JSON.stringify(tempBooks));
  }

  showDeleteAlert(book) {
    Alert.alert('Deletion', 'Sure to delete ' + book.title + ' ?', [
      { text: 'Cancel', onPress: () => { } },
      { text: 'OK', onPress: () => this.deleteBook(book) },
    ]);
  }

  render() {
    return (
      <View style={{ flex: 1, paddingTop: 45 }}>
        <TextInput
          value={this.state.newTitle}
          onChangeText={value => this.setState({ newTitle: value })}
          />
        <TextInput
          value={this.state.newAuthor}
          onChangeText={value => this.setState({ newAuthor: value })}
          />
        <TouchableHighlight onPress={
          () => {
            this.saveBook();
          }
        }>
          <Text style={{ backgroundColor: 'green', color: 'white' }}>Create Book</Text>
        </TouchableHighlight>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={(rowData) =>
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <TouchableOpacity style={{ flex: 3 }} onPress={
                () => {
                  this.props.navigator.push({
                    index: 2,
                    passProps: {
                      id: rowData.id,
                    }
                  })//push
                }
              }>
                <Text>{rowData.title}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ flex: 1 }} onPress={
                () => {
                  //this.deleteBook(rowData);
                  this.showDeleteAlert(rowData);
                }
              }>
                <Text style={{ color: 'red' }}>X</Text>
              </TouchableOpacity>
            </View>
          }
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  save: {
    backgroundColor: 'green'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
