import React, { Component } from 'react';
import {
    TouchableHighlight,
    Image,
    AppRegistry,
    StyleSheet,
    Text,
    TextInput,
    View,
    Picker,
    AsyncStorage
} from 'react-native';

export default class BookDetail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editedTitle: '',
            editedAuthor: '',
            id: props.id
        };

        this.getBook(props.id).done();
    }

    async getBook(id) {
        await AsyncStorage.getItem('@AwesomeProject:allBooks').then((value) => {
            let books = JSON.parse(value);
            for (var i = 0; i < books.length; ++i) {
                let crtBook = books[i];
                if (crtBook.id === id) {
                    this.setState({ editedTitle: crtBook.title });
                    this.setState({ editedAuthor: crtBook.author });
                }
            }
            //this.setState({ editedTitle: 'Bobby'});
            //alert('AsyncStorage');
        });
    }

    updateBook() {
        AsyncStorage.getItem('@AwesomeProject:allBooks').then((value) => {
            let books = JSON.parse(value);
            for (var i = 0; i < books.length; ++i) {
                let crtBook = books[i];
                if (crtBook.id === this.state.id) {
                    crtBook.title = this.state.editedTitle;
                    crtBook.author = this.state.editedAuthor;
                }
            }
            AsyncStorage.setItem('@AwesomeProject:allBooks', JSON.stringify(books)).done();
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <Text> Book Id: {this.props.id}</Text>
                <TextInput
                    value={this.state.editedTitle}
                    onChangeText={value => this.setState({ editedTitle: value })}
                    />
                <Picker selectedValue={this.state.editedAuthor}
                    onValueChange={(value) => this.setState({ editedAuthor: value })}>
                    <Picker.Item label="1. Clark" value="Clark" />
                    <Picker.Item label="2. Asimov" value="Asimov" />
                </Picker>

                <TouchableHighlight onPress={
                    () => {
                        this.updateBook();
                    }
                }>
                    <Text style={{ backgroundColor: 'green', color: 'white' }}>Save Changes</Text>
                </TouchableHighlight>

            </View>
        );
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        paddingTop: 70,
    },
});
