import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert,
  TextInput,
  ListView,
  TouchableOpacity,
} from 'react-native';

'use strict';
const ReactNative = require('react-native');
import Button from 'react-native-button';

export default class CollectionComponent extends Component {
  // Initialize the hardcoded data
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      dataSource: ds.cloneWithRows([
        { name: "Clark", age: 20 },
        { name: "Martha", age: 45 },
        { name: "Lex", age: 18 }
      ])
    };
  }
  render() {
    return (
      <View style={{ flex: 1, paddingTop: 45 }}>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={(rowData) =>
            <TouchableOpacity onPress={
              () => {
                //Alert.alert("pushed");
                this.props.navigator.push({
                  index: 2,
                  passProps: {
                    name: rowData.name,
                    age: rowData.age
                  }
                })//push
              }
            }>
              <Text>{rowData.name}</Text>
            </TouchableOpacity>
          }
          // renderRow={ (rowData) => {
          //   <TouchableOpacity
          //     // onPress={
          //     //   () => this.props.navigator.push({
          //     //   index: 1,
          //     //   passProps: {
          //     //     symbol: data.symbol,
          //     //     date: data.values[0].date,
          //     //     val: data.values[0].val,
          //     //   }
          //     // })
          //     // }
          //     >
          //     <View>
          //       <Text>{rowData}</Text>
          //     </View>
          //   </TouchableOpacity>
          // }//renderRow
          //}
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
