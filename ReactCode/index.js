import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert,
  TextInput,
  Navigator,
  StatusBar,
  TouchableHighlight,
} from 'react-native';

'use strict';
const ReactNative = require('react-native');
import Button from 'react-native-button';
import MailComponent from './mailComponent.js';
//import CollectionComponent from './collectionComponent.js';
import BookCollection from './bookCollection.js';
//import DetailComponent from './detailComponent.js';
import BookDetail from './bookDetail.js';

const routes = [
  {
    title: 'Mail',
    index: 0
  }, {
    //title: 'Collection',
    title: 'Book Collection',
    index: 1
  },{
    title: 'Book Detail',
    index: 2
  }
]

export default class IndexComponent extends Component {
  sceneNumber = 3;
  render() {
    return (
      <Navigator
        initialRoute={routes[0]}
        initialRouteStack={routes}
        renderScene={
          (route, navigator) => {
            switch (route.index) {
              case 0: return (<MailComponent navigator={navigator} route={routes[route.index]} {...route.passProps}></MailComponent>);
              //case 1: return (<CollectionComponent navigator={navigator} route={routes[route.index]} {...route.passProps}></CollectionComponent>);
              case 1: return (<BookCollection navigator={navigator} route={routes[route.index]} {...route.passProps}></BookCollection>);
              //case 2: return (<DetailComponent navigator={navigator} route={routes[route.index]} {...route.passProps}></DetailComponent>);
              case 2: return (<BookDetail navigator={navigator} route={routes[route.index]} {...route.passProps}></BookDetail>);
            }
          }
        }
        configureScene={
          (route, routeStack) =>
            Navigator.SceneConfigs.FloatFromRight
        }
        navigationBar={
          <Navigator.NavigationBar
            routeMapper={{
              LeftButton: (route, navigator, index, navState) => {
                if (route.index == 0) {
                  return null;
                }
                return (
                  <TouchableHighlight onPress={
                    () => {
                      navigator.pop();
                    }
                  }>
                    <Text style={styles.navigationBarText}>Back</Text>
                  </TouchableHighlight>
                )
              },
              RightButton: (route, navigator, index, navState) => {
                // if (route.index == routes.length - 1) {
                if (route.index >= routes.length - 2) {
                  return null;
                }
                return (
                  <TouchableHighlight onPress={
                    () => {
                      const nextIndex = route.index + 1;
                      navigator.push(routes[nextIndex]);
                    }
                  }>
                    <Text style={styles.navigationBarText}>Go To List</Text>
                  </TouchableHighlight>
                )
              },
              Title: (route, navigator, index, navState) =>
              { return (<Text style={[styles.navigationBarText, styles.titleText]}>{routes[route.index].title}</Text>); },
            }}
            style={styles.navigationBar}
            />
        }
        />
    );

  }
}


/*
 <View style={styles.container}>
        <Text style={styles.welcome}>
          BLABLABLA
        </Text>
        <Text style={styles.instructions}>
         $---------|IndexComponent|-------
        </Text>
        <Text style={styles.instructions}>
          Double tap R on your keyboard to reload,{'\n'}
          Shake or press menu button for dev menu
        </Text>
        <MailComponent/>
      </View>
*/

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
