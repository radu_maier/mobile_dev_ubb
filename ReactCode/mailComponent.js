import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert,
  TextInput
} from 'react-native';

'use strict'; 
 const ReactNative = require('react-native');
import Button from 'react-native-button';


export default class MailComponent extends Component
{
        constructor(props) {
            super(props)

            this.state = {
             mailBody: '',
            }
        }

      _handlePress() {
          const { mailBody} = this.state;
            //Alert.alert('Button has been pressed!'   + mailBody);
            var SendIntentAndroid = require('react-native-send-intent');
            SendIntentAndroid.sendMail("radumaier01@gmail.com", "Email From ReactNative App", mailBody);
        }
    render()
    {
        return(
            <View>
            <TextInput
                value={this.state.mailBody}
                onChangeText={mailBody => this.setState({mailBody})}
                />
             <Button
                autoFocus = {true}
                style={{fontSize: 20, color: 'green'}}
                styleDisabled={{color: 'red'}}
                onPress={() => this._handlePress()}>
                Send Email
            </Button>
            </View>
        );
    }
}


