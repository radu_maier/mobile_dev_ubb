package com.example.razor.ubbapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;

/**
 * Created by razor on 12/20/2016.
 */
public class BookOnItemClickListener implements AdapterView.OnItemClickListener {
    private final AppCompatActivity _callingActivity;
    private final Intent _intent;

    public BookOnItemClickListener(AppCompatActivity callingActivity, Intent intent) {
        this._callingActivity = callingActivity;
        this._intent = intent;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Book selectedBook = (Book)adapterView.getItemAtPosition(position);
        //String itemPicked = "You selected: " + String.valueOf(selected.age);
        //Toast.makeText(ShowListActivity.this, itemPicked,Toast.LENGTH_SHORT).show();
        Intent detailIntent =  _intent;
        int bookId = (int) selectedBook.getId();
        detailIntent.putExtra(ShowListActivity.EXTRA_BOOK_ID, bookId);
        _callingActivity.startActivity(detailIntent);
    }
}
