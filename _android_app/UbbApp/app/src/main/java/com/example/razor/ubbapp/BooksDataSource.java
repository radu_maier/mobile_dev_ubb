package com.example.razor.ubbapp;

/**
 * Created by razor on 12/19/2016.
 */

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class BooksDataSource {

    // Database fields
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = { MySQLiteHelper.COLUMN_ID,
            MySQLiteHelper.COLUMN_TITLE, MySQLiteHelper.COLUMN_AUTHOR };

    public BooksDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Book createBook(String title, String author) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_TITLE, title);
        values.put(MySQLiteHelper.COLUMN_AUTHOR, author);
        long insertId = database.insert(MySQLiteHelper.TABLE_BOOKS, null,
                values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_BOOKS,
                allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Book newBook = cursorToBook(cursor);
        cursor.close();
        return newBook;
    }

    public void deleteBook(Book book) {
        long id = book.getId();
        database.delete(MySQLiteHelper.TABLE_BOOKS, MySQLiteHelper.COLUMN_ID
                + " = " + id, null);
    }

    public Book getBook(int id){
        String rawQuery = "select * from " +MySQLiteHelper.TABLE_BOOKS + " where "
                + MySQLiteHelper.COLUMN_ID + "=" + id + "";
        Cursor cursor = database.rawQuery(rawQuery , null);
        cursor.moveToFirst();
        Book book = cursorToBook(cursor);
        return book;
    }

    public List<Book> getAllBooks() {
        List<Book> books = new ArrayList<Book>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_BOOKS,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Book book = cursorToBook(cursor);
            books.add(book);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return books;
    }

    private Book cursorToBook(Cursor cursor) {
        Book book = new Book();
        book.setId(cursor.getLong(0));
        book.setTitle(cursor.getString(1));
        book.setAuthor(cursor.getString(2));
        return book;
    }

    public void updateBook(Book modifiedBook) {
        int id = (int)modifiedBook.getId();

        ContentValues cv = new ContentValues();
        cv.put(MySQLiteHelper.COLUMN_TITLE,modifiedBook.getTitle()); //These Fields should be your String values of actual column names
        cv.put(MySQLiteHelper.COLUMN_AUTHOR,modifiedBook.getAuthor());

        int rowsAffected = database.update(MySQLiteHelper.TABLE_BOOKS, cv, "_id="+id, null);
    }
}