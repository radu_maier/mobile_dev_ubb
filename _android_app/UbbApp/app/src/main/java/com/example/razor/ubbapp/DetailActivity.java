package com.example.razor.ubbapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent intent = getIntent();
        String name = intent.getStringExtra(ShowListActivity.EXTRA_NAME);
        int age =  intent.getIntExtra(ShowListActivity.EXTRA_AGE, -1);

//        TextView textView = new TextView(this);
//        textView.setTextSize(40);
//        textView.setText("Name: "+name);
//
//        TextView textView2 = new TextView(this);
//        textView2.setTextSize(40);
//        textView2.setText("Age: "+age);

        TextView tw1 = (TextView) findViewById(R.id.name);
        TextView tw2 = (TextView) findViewById(R.id.age);
        tw1.setTextSize(40);
        tw1.setText("Name: "+name);
        tw2.setTextSize(40);
        tw2.setText("Age: "+age);

        ViewGroup layout = (ViewGroup) findViewById(R.id.activity_detail);
        //layout.addView(tw1);
        //layout.addView(tw2);
    }

    ///Called when the delete button is pushed
    public void deleteBook(View view) {

    }
}
