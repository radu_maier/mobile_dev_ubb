package com.example.razor.ubbapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.List;

public class DetailBookAcrivity extends AppCompatActivity {

    private BooksDataSource datasource;
    private Book selectedBook;
    private DetailBookAcrivity self = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_book_acrivity);
        datasource = new BooksDataSource(this);
        datasource.open();
        Intent intent = getIntent();
        int id =  intent.getIntExtra(ShowListActivity.EXTRA_BOOK_ID,-1 );
        selectedBook = datasource.getBook(id);

        EditText editTextTitle = (EditText) findViewById(R.id.edit_title);
        EditText editTextAuthor = (EditText) findViewById(R.id.edit_author);

        editTextTitle.setText(selectedBook.getTitle());
        editTextAuthor.setText(selectedBook.getAuthor());

        setUpChart();


    }

    private void setUpChart() {
        LineChart chart = (LineChart) findViewById(R.id.chart);

        List<Entry> entries = new ArrayList<Entry>();

        entries.add(new Entry(1,1));
        entries.add(new Entry(2,1));

        LineDataSet dataSet = new LineDataSet(entries, "Label"); // add entries to dataset
        //dataSet.setColor();
        //dataSet.setValueTextColor(...);
        LineData lineData = new LineData(dataSet);
        chart.setData(lineData);
        chart.invalidate(); // refresh
    }

    public void saveChanges(View view)
    {
        EditText editTextTitle = (EditText) findViewById(R.id.edit_title);
        EditText editTextAuthor = (EditText) findViewById(R.id.edit_author);

        String newTitle = editTextTitle.getText().toString();
        String newAuthor = editTextAuthor.getText().toString();

        selectedBook.setTitle(newTitle);
        selectedBook.setAuthor(newAuthor);
        datasource.updateBook(selectedBook);

        showToast("Update done!");
    }

    private void showToast(String msg) {
        Context context = getApplicationContext();
        CharSequence text = msg;
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    public void eraseBook(View view)
    {
        Context context = getApplicationContext();

        new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.AppTheme))
                .setTitle("Delete entry")
                .setMessage("Are you sure you want to delete this entry?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        datasource.deleteBook(selectedBook);
                        self.finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    protected void onResume() {
        datasource.open();
        super.onResume();
    }

    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }
}