package com.example.razor.ubbapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import static java.net.Proxy.Type.HTTP;

public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

     /** Called when the user clicks the Send button */
    public void sendMessage(View view) {

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        // The intent does not have a URI, so declare the "text/plain" MIME type
        //emailIntent.setType(HTTP.PLAIN_TEXT_TYPE);
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {"radumaier01@gmail.com"}); // recipients
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Android Studio App");

        EditText editText = (EditText) findViewById(R.id.edit_message);
        String message = editText.getText().toString();
        emailIntent.putExtra(Intent.EXTRA_TEXT, message);

        startActivity(Intent.createChooser(emailIntent, "Send mail..."));//choose which email address


        /*
        Intent intent = new Intent(this, DisplayMsgActivity.class);
        EditText editText = (EditText) findViewById(R.id.edit_message);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
        */
    }


    public void showList(View view){
        Intent intent = new Intent(this, ShowListActivity.class);
        startActivity(intent);
    }


}
