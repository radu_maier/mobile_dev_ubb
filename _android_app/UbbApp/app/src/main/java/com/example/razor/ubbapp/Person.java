package com.example.razor.ubbapp;

/**
 * Created by razor on 11/20/2016.
 */

public class Person {
    public String name;
    public int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
