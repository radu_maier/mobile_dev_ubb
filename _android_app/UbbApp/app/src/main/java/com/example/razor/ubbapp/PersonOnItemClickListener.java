package com.example.razor.ubbapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;

/**
 * Created by razor on 11/20/2016.
 */

public class PersonOnItemClickListener implements AdapterView.OnItemClickListener {
    public Intent _intent;
    public AppCompatActivity _callerActivity;
@Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Person selected = (Person)adapterView.getItemAtPosition(position);
        //String itemPicked = "You selected: " + String.valueOf(selected.age);
        //Toast.makeText(ShowListActivity.this, itemPicked,Toast.LENGTH_SHORT).show();
        Intent detailIntent =  _intent;

        detailIntent.putExtra(ShowListActivity.EXTRA_NAME, selected.name);
        detailIntent.putExtra(ShowListActivity.EXTRA_AGE, selected.age);
        _callerActivity.startActivity(detailIntent);
    }
}
