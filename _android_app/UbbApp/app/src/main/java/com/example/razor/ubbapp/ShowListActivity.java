package com.example.razor.ubbapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
//import android.database.sqlite.;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class ShowListActivity extends AppCompatActivity {

    public final static String EXTRA_NAME = "com.example.myfirstapp.DETAIL_NAME";
    public final static String EXTRA_AGE = "com.example.myfirstapp.DETAIL_AGE";
    public final static String EXTRA_BOOK_ID = "com.example.myfirstapp.BOOK_ID";

    private ArrayAdapter<Book> bookAdapter;

    private BooksDataSource datasource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //some dummy data, below we configure the list of books
/*
        setContentView(R.layout.activity_show_list);
        //String[] items = {"Bruce","Lois"};
        Person[] persons  = {new Person("Bob",23), new Person("Jill",29)};

        ListAdapter theAdapter = new ArrayAdapter<Person>(this,
                android.R.layout.simple_expandable_list_item_1,persons
        );
        ListView theListView = (ListView) findViewById(R.id.theListView);
        theListView.setAdapter(theAdapter);

        PersonOnItemClickListener listener = new PersonOnItemClickListener();
        listener._callerActivity = this;
        listener._intent = new Intent(this,DetailActivity.class);

        theListView.setOnItemClickListener(listener);
    */
        //end some dummy data

        //list of books

        setContentView(R.layout.activity_show_list);
        datasource = new BooksDataSource(this);
        datasource.open();

        List<Book> values = datasource.getAllBooks();

        // use the SimpleCursorAdapter to show the
        // elements in a ListView
         bookAdapter = new ArrayAdapter<Book>(this,
                android.R.layout.simple_expandable_list_item_1, values);
        ListView bookListView = (ListView) findViewById(R.id.theListView);
        bookListView.setAdapter(bookAdapter);

        BookOnItemClickListener clickListener = new BookOnItemClickListener(this, new Intent(this,DetailBookAcrivity.class) );
        bookListView.setOnItemClickListener(clickListener);

        //Book book = datasource.createBook("Harry Potter", "J. K. Rowling");

        //bookAdapter.add(book);
       // bookAdapter.notifyDataSetChanged();

    }

    public void createBook(View view){
        EditText titleET = ((EditText) findViewById(R.id.book_title));
        String title = titleET.getText().toString();
        titleET.setText("Title...");

        EditText authorET = ((EditText) findViewById(R.id.book_author));
        String author = authorET.getText().toString();
        authorET.setText("Author...");

        if(title.isEmpty())
            return;

        if(author.isEmpty())
            return;

        Book book = datasource.createBook(title, author);

        bookAdapter.add(book);
        bookAdapter.notifyDataSetChanged();


    }

    @Override
    protected void onResume() {
        datasource.open();
        super.onResume();
        bookAdapter.clear();
        List<Book> values = datasource.getAllBooks();
        bookAdapter.addAll(values);
    }

    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }
}
