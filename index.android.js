/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View, Alert
} from 'react-native';
import IndexComponent from './ReactCode/index.js'

const onClick_ = () => { Alert.alert('Button has been pressed!'); };

export default class AwesomeProject extends Component {
  render() {
    return (
       <IndexComponent/>
    );
  }
}

AppRegistry.registerComponent('AwesomeProject', () => AwesomeProject);
